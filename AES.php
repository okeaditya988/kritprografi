<?php
  require 'php/AES.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>RSA AES Combination Cryptography</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">RSA AES Combination Cryptography</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="AES">AES</a>
					</li>
					<li><a href="RSA">RSA</a>
					</li>
					<li><a href="Combine">Combine</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>RSA AES Combination Cryptography</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-xs-12">
						<div class="exhibit">
							<div class="form-group">
								<label for="AESpasswordJS">Password</label>
								<input type="text" class="form-control" id="AESpasswordJS" name="AESpasswordJS">
							</div>
							<div class="form-group">
								<label for="AESencryptJS">Encrypt</label>
								<textarea class="form-control" id="AESencryptJS" name="AESencryptJS"
									rows="1"></textarea>
								<button type="submit" class="btn btn-default" id="JSencryptAES">Encrypt</button>
							</div>
							<div class="form-group">
								<label for="AESencryptedJS">Encrypted</label>
								<textarea class="form-control rdonly" id="AESencryptedJS" name="AESencryptedJS" rows="3"
									readonly></textarea>
								<button type="submit" class="btn btn-default" id="AES-JStoJS">JS<span
										class="glyphicon glyphicon-arrow-down"></span>
								</button>
							</div>
							<div class="form-group">
								<label for="AESdecryptJS">Decrypt</label>
								<textarea class="form-control rdonly" id="AESdecryptJS" name="AESdecryptJS" rows="3"
									readonly></textarea>
								<button type="submit" class="btn btn-default" id="JSdecryptAES">Decrypt</button>
							</div>
							<div class="form-group">
								<label for="AESdecryptedJS">Decrypted</label>
								<textarea class="form-control rdonly" id="AESdecryptedJS" name="AESdecryptedJS" rows="1"
									readonly></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/aes.min.js"></script>
	<script>
		$(function () {
			$("#JSencryptAES").click(function (e) {
				$("#AESencryptedJS").html(""), $("#JSencryptAES").blur();
				var t = $("#AESencryptJS").val(),
					r = $("#AESpasswordJS").val(),
					S = GibberishAES.enc(t, r);
				$("#AESencryptedJS").val(S), e.preventDefault()
			}), $("#JSdecryptAES").click(function (e) {
				$("#AESdecryptedJS").html(""), $("#JSdecryptAES").blur();
				var t = $("#AESdecryptJS").val(),
					r = $("#AESpasswordJS").val(),
					S = GibberishAES.dec(t, r);
				$("#AESdecryptedJS").val(S), e.preventDefault()
			}), $("#AES-JStoJS").click(function (e) {
				$("#AESdecryptJS").val($("#AESencryptedJS").val()), e.preventDefault(), $("#AES-JStoJS")
				.blur()
			})
		});
	</script>
</body>

</html>