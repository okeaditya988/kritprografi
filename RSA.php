<?php
  ini_set('max_execution_time', 300);
  require 'php/RSA.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>RSA AES Combination Cryptography</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">RSA AES Combination Cryptography</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="AES">AES</a>
					</li>
					<li><a href="RSA">RSA</a>
					</li>
					<li><a href="Combine">Combine</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1>RSA AES Combination Cryptography</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								Key Pair Generation
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="col-xs-6">
								<div class="exhibit">
									<div class="btn-group bottom-pad">
										<button class="btn btn-default dropdown-toggle" id="RSAkeysizeJS" type="button"
											data-value="1024" data-toggle="dropdown">1024 bit <span
												class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<li><a class="change-JS-key-size" data-value="512" href="#">512 bit</a>
											</li>
											<li><a class="change-JS-key-size" data-value="1024" href="#">1024 bit</a>
											</li>
											<li><a class="change-JS-key-size" data-value="2048" href="#">2048 bit</a>
											</li>
											<li><a class="change-JS-key-size" data-value="4096" href="#">4096 bit</a>
											</li>
										</ul>
									</div>
									<div class="form-group">
										<label for="JSpublicRSA">Public Key</label>
										<textarea class="form-control rdonly" id="JSpublicRSA" rows="3"
											readonly></textarea>
									</div>
									<div class="form-group">
										<label for="JSprivateRSA">Private Key</label>
										<textarea class="form-control rdonly" id="JSprivateRSA" rows="3"
											readonly></textarea>
									</div>
									<button type="submit" class="btn btn-default" id="JSkeypairRSA">Generate Key
										Pair</button>
									<div id="JSloadingRSA" class="loading" style="display:none">Generating...</div>
									<button type="submit" class="btn btn-default fl-right" id="JSkeyuseRSA">Use <span
											class="glyphicon glyphicon-arrow-down"></span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								Encryption / Decryption
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="col-xs-6">
								<div class="exhibit">
									<div class="form-group">
										<label for="public">Public Key</label>
										<textarea class="form-control" id="pubkey" name="pubkey" rows="4"></textarea>
									</div>
									<div class="form-group">
										<label for="private">Private Key</label>
										<textarea class="form-control" id="prikey" name="prikey" rows="4"></textarea>
									</div>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="exhibit">
									<div class="form-group">
										<label for="RSAencryptJS">Encrypt</label>
										<textarea class="form-control" id="RSAencryptJS" name="RSAencryptJS"
											rows="1"></textarea>
										<button type="submit" class="btn btn-default" id="JSencryptRSA">JS
											Encrypt</button>
									</div>
									<div class="form-group">
										<label for="RSAencryptedJS">Encrypted</label>
										<textarea class="form-control rdonly" id="RSAencryptedJS" name="RSAencryptedJS"
											rows="3" readonly></textarea>
										<button type="submit" class="btn btn-default" id="RSA-JStoJS">JS<span
												class="glyphicon glyphicon-arrow-down"></span>
										</button>
									</div>
									<div class="form-group">
										<label for="RSAdecryptJS">Decrypt</label>
										<textarea class="form-control" id="RSAdecryptJS" name="RSAdecryptJS"
											rows="3"></textarea>
										<button type="submit" class="btn btn-default" id="JSdecryptRSA">JS
											Decrypt</button>
									</div>
									<div class="form-group">
										<label for="RSAdecryptedJS">Decrypted</label>
										<textarea class="form-control rdonly" id="RSAdecryptedJS" name="RSAdecryptedJS"
											rows="1" readonly></textarea>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/rsa.min.js"></script>

	<script type="text/javascript">
		$(function () {
			$(".change-JS-key-size").each(function (e, t) {
				var a = $(t),
					r = a.attr("data-value");
				a.click(function (e) {
					var t = $("#RSAkeysizeJS");
					t.attr("data-value", r), t.html(r + ' bit <span class="caret"></span>'), e
						.preventDefault()
				})
			}), $("#JSkeypairRSA").click(function (e) {
				$("#JSloadingRSA").show(function () {
					var e = $("#RSAkeysizeJS").attr("data-value"),
						t = parseInt(e),
						a = forge.pki.rsa.createKeyPairGenerationState(t, 65537),
						r = function () {
							forge.pki.rsa.stepKeyPairGenerationState(a, 1e3) ? ($("#JSpublicRSA").val(
									forge.pki.publicKeyToPem(a.keys.privateKey)), $(
									"#JSprivateRSA").val(forge.pki.privateKeyToPem(a.keys
									.privateKey)), $("#JSloadingRSA").hide(), $("#JSkeypairRSA")
								.blur()) : setTimeout(r, 1)
						};
					setTimeout(r, 0)
				}), e.preventDefault()
			}), $("#JSkeyuseRSA").click(function (e) {
				$("#JSprivateRSA").change(function () {
					$("#prikey").val($("#JSprivateRSA").val()), $("#pvtkey").val($("#JSprivateRSA")
						.val())
				}).change(), $("#JSpublicRSA").change(function () {
					$("#pubkey").val($("#JSpublicRSA").val()), $("#pbckey").val($("#JSpublicRSA")
					.val())
				}).change(), e.preventDefault(), $("#JSkeyuseRSA").blur()
			}), $("#JSencryptRSA").click(function (e) {
				$("#RSAencryptedJS").val("");
				var t = $("#pubkey").val(),
					a = forge.pki.publicKeyFromPem(t),
					r = $("#RSAencryptJS").val(),
					i = a.encrypt(r),
					S = forge.util.encode64(i);
				$("#RSAencryptedJS").val(S), e.preventDefault(), $("#JSencryptRSA").blur()
			}), $("#JSdecryptRSA").click(function (e) {
				$("#RSAdecryptedJS").val("");
				var t = $("#prikey").val(),
					a = forge.pki.privateKeyFromPem(t),
					r = $("#RSAdecryptJS").val(),
					i = forge.util.decode64(r),
					S = a.decrypt(i);
				$("#RSAdecryptedJS").val(S), e.preventDefault(), $("#JSdecryptRSA").blur()
			}), $("#RSA-JStoJS").click(function (e) {
				$("#RSAdecryptJS").val($("#RSAencryptedJS").val()), e.preventDefault(), $("#RSA-JStoJS")
				.blur()
			})
		});
	</script>
</body>

</html>